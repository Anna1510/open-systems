# Лабораторная работа № 5

### Работа с регулярными выражениями

## Описание работы

* Необходимо распарсить bib файл

* В данном файле могут находиться названия как на русском, так и на английском языках

* Могут встречаться спец. символы, например **\n** (символ переноса строки) в значениях полей

## Особенности реализации

* Если встречается **\n** в значении поля, например в данной записи в полях Title и Author, то при записи в итоговый файл он заменится на пустую строку

> @Article{demouchy,
> 
>   Title                    = { Diffusion and thermodiffusion studies in
> ferrofluids with a new two-dimensional
> 
> forced Rayleigh-scattering technique},
> 
>   Author                   = { Demouchy, G. and Mezulis, A. and Bee, A. D. and Talbot, D. Bacri, J. C. and
> 
>  Bourdon, A.
> 
> },
> 
>   Journal                  = {J. Phys. D: Appl. Phys.},
> 
>   Year                     = {2004},
> 
>   Number                   = {10},
> 
>   Pages                    = {1417-1428},
> 
>   Volume                   = {37}
> 
> }

* Имеется список всех полей для каждого доступного вида документа, список их разделителей (для английского и русского варантов), шаблоны для формирования записей по ГОСТу

```python
fields = {
    'Article': ['Title', 'Author', 'Journal', 'Year', 'Pages',
                'Volume', 'Language', 'Address'],
    'Book': ['Title', 'Author', 'Publisher', 'Year', 'Numpages', 'Language', 'Address'],
    'Booklet': ['Title'],
    'PhdThesis': ['Title', 'Author', 'School', 'Year', 'Address', 'Language'],
    'Conference': ['Title', 'Author', 'Booktitle', 'Year', 'Address', 'Pages', 'Language']
}
```

```python
separators_ru = {
    'Title': '//',
    'Author': ' ',
    'Journal': ' ',
    'Year': '-',
    'Pages': '-С',
    'Volume': '-Вып',
    'Publisher': 'Изд-во',
    'Address': ' ',
    'Booktitle': ' ',
    'Numpages': '-С',
    'School': ' '
}

separators_en = {
    'Title': '//',
    'Author': ' ',
    'Journal': ' ',
    'Year': '-',
    'Pages': '-P',
    'Volume': '-Vol',
    'Publisher': 'Pub',
    'Address': ' ',
    'Booktitle': ' ',
    'Numpages': '-P',
    'School': ' '
}
```

```python
templates = {
    'Article': '{Author}{Title}{Journal}{Address}{Year}{Volume}{Pages}',
    'Book': '{Author}{Title}{Address}{Publisher}{Year}{Numpages}',
    'Booklet': '{Title}',
    'PhdThesis': '{Author}{Title}{Address}{School}{Year}',
    'Conference': '{Author}{Title}{Booktitle}{Year}{Address}'
}
```