import sqlite3
from parsing import parser


class Record:
    """
    Base class for all records
    """
    def write(self):
        return ''


class Article(Record):
    """
    Class for article record
    """
    def write(self):
        """
        Forms an sql string for inserting an article record to a db
        :return: string
        """
        return 'INSERT INTO Articles (id, author, title, journal, address, year, volume, pages) VALUES (?, ?, ?, ?, ?, ?, ?, ?)', \
               (self.id, self.author, self.title, self.journal, self.address, self.year, self.volume, self.pages)

    def __init__(self, field_values, it):
        """
        Constructor for Article class
        :param field_values: dictionary where key - name of field, value - value of this field
        :param it: index of item (used for id)
        """
        self.id = it
        self.author = field_values['Author']
        self.title = field_values['Title']
        self.journal = field_values['Journal']
        self.address = field_values['Address']
        self.year = field_values['Year']
        while not self.year.isdigit():
            self.year = self.year[1:]
        self.volume = field_values['Volume']
        self.pages = field_values['Pages']
        return


class Book(Record):
    def write(self):
        """
        Forms an sql string for inserting an article record to a db
        :return: string
        """
        return 'INSERT INTO Books (id, author, title, publisher, address, year, numpages) VALUES (?, ?, ?, ?, ?, ?, ?)', \
               (self.id, self.author, self.title, self.publisher, self.address, self.year, self.numpages)

    def __init__(self, field_values, it):
        """
        Constructor for Book class
        :param field_values: dictionary where key - name of field, value - value of this field
        :param it: index of
        """
        self.id = it
        self.author = field_values['Author']
        self.title = field_values['Title']
        self.publisher = field_values['Publisher']
        self.address = field_values['Address']
        self.year = field_values['Year']
        while not self.year.isdigit():
            self.year = self.year[1:]
        self.numpages = field_values['Numpages']
        return


class Booklet(Record):
    def write(self):
        """
        Forms an sql string for inserting an article record to a db
        :return: string
        """
        return 'INSERT INTO Booklets (id, title) VALUES (?, ?)', (self.id, self.title)

    def __init__(self, field_values, it):
        """
        Constructor for Booklet class
        :param field_values: dictionary where key - name of field, value - value of this field
        :param it: index of
        """
        self.id = it
        self.title = field_values['Title']
        pass


class PhdThesis(Record):
    def write(self):
        """
        Forms an sql string for inserting an article record to a db
        :return: string
        """
        return 'INSERT INTO PhdTheses (id, author, title, school, address, year) VALUES (?, ?, ?, ?, ?, ?)', \
               (self.id, self.author, self.title, self.school, self.address, self.year)

    def __init__(self, field_values, it):
        """
        Constructor for PhdThesis class
        :param field_values: dictionary where key - name of field, value - value of this field
        :param it: index of
        """
        self.id = it
        self.title = field_values['Title']
        self.author = field_values['Author']
        self.school = field_values['School']
        self.year = field_values['Year']
        while not self.year.isdigit():
            self.year = self.year[1:]
        self.address = field_values['Address']
        return


class Conference(Record):
    def write(self):
        """
        Forms an sql string for inserting an article record to a db
        :return: string
        """
        return 'INSERT INTO Conferences (id, author, title, booktitle, address, year, pages) VALUES (?, ?, ?, ?, ?, ?, ?)', \
               (self.id, self.author, self.title, self.booktitle, self.address, self.year, self.pages)

    def __init__(self, field_values, it):
        """
        Constructor for Conference class
        :param field_values: dictionary where key - name of field, value - value of this field
        :param it: index of
        """
        self.id = it
        self.title = field_values['Title']
        self.author = field_values['Author']
        self.booktitle = field_values['Booktitle']
        self.year = field_values['Year']
        while not self.year.isdigit():
            self.year = self.year[1:]
        self.address = field_values['Address']
        self.pages = field_values['Pages']
        return


class SQLWriter:
    """
    Class for db interaction
    """
    def __init__(self, connection_string, table_names):
        """
        Constructor for establishing db connection and table creation
        :param connection_string: path to db
        :param table_names: array of string with db tables
        """
        self.connection = sqlite3.connect(connection_string)
        self.cursor = self.connection.cursor()

        self.cursor.execute(f'DROP TABLE IF EXISTS {table_names[0]}')  # Articles
        self.cursor.execute(f'DROP TABLE IF EXISTS {table_names[1]}')  # Books
        self.cursor.execute(f'DROP TABLE IF EXISTS {table_names[2]}')  # Booklets
        self.cursor.execute(f'DROP TABLE IF EXISTS {table_names[3]}')  # PhdTheses
        self.cursor.execute(f'DROP TABLE IF EXISTS {table_names[4]}')  # Conferences

        self.cursor.execute(f'CREATE TABLE {table_names[0]} ('
                            f'id int primary key,'
                            f'author varchar(200),'
                            f'title varchar(200),'
                            f'journal varchar(200),'
                            f'address varchar(200),'
                            f'year int,'
                            f'pages varchar(50),'
                            f'volume varchar(50))')

        self.cursor.execute(f'CREATE TABLE {table_names[1]} ('
                            f'id int primary key,'
                            f'author varchar(200),'
                            f'title varchar(200),'
                            f'publisher varchar(200),'
                            f'address varchar(200),'
                            f'year int,'
                            f'numpages varchar(50))')

        self.cursor.execute(f'CREATE TABLE {table_names[2]} ('
                            f'id int primary key,'
                            f'title varchar(200))')

        self.cursor.execute(f'CREATE TABLE {table_names[3]} ('
                            f'id int primary key,'
                            f'author varchar(200),'
                            f'title varchar(200),'
                            f'school varchar(200),'
                            f'address varchar(200),'
                            f'year int)')

        self.cursor.execute(f'CREATE TABLE {table_names[4]} ('
                            f'id int primary key,'
                            f'author varchar(200),'
                            f'title varchar(200),'
                            f'booktitle varchar(200),'
                            f'address varchar(200),'
                            f'year int,'
                            f'pages varchar(50))')
        return

    def write(self, item: Record):
        """
        Inserts new record to db under transaction
        :param item: what we insert
        :return: nothing
        """
        try:
            self.cursor.execute('BEGIN TRANSACTION')
            self.cursor.execute(item.write()[0], item.write()[1])
            self.cursor.execute('COMMIT')
        except sqlite3.Error as e:
            print(e)
            self.cursor.execute('ROLLBACK')
        return

    def select(self, author, **kwargs):
        """
        Select an article from db
        :param author: author of an article
        :param kwargs: year and journal - optional restrictions
        :return: list of articles
        """
        year_addition = ''
        journal_addition = ''
        _year = kwargs['year']
        _journal = kwargs['journal']
        _author = '%' + author + '%'
        params = [_author]
        if _year != '':
            year_addition = """ AND year == ?"""
            params.append(_year)
        if _journal != '':
            journal_addition = """ AND journal LIKE ?"""
            params.append(_journal)
        statement = """SELECT id, author, title, journal, address, year, volume, pages 
                                        FROM Articles WHERE author LIKE ?""" + year_addition + journal_addition
        output = self.cursor.execute(statement, params).fetchall()
        return output


def main():
    """
    Main function
    :return:
    """
    writer = SQLWriter('lab.db', ['Articles', 'Books', 'Booklets', 'PhdTheses', 'Conferences'])
    data = parser()
    records = []

    for i, record in enumerate(data):
        if record['type'] == 'Article':
            record = Article(record, i)
        elif record['type'] == 'Book':
            record = Book(record, i)
        elif record['type'] == 'PhdThesis':
            record = PhdThesis(record, i)
        elif record['type'] == 'Conference':
            record = Conference(record, i)
        elif record['type'] == 'Booklet':
            record = Booklet(record, i)
        records.append(record)

    for record in records:
        writer.write(record)

    print('Введите имя автора, статью которого хотите найти')
    author = input()
    year = ''
    journal = ''
    print('Хотите добавить фильтр по году издания или журналу? [Y/N]')
    filters = True if input().upper() == 'Y' else False
    if filters:
        print('Введите год, который хотите выбрать')
        year = input()
        print('Введите журнал, который хотите выбрать')
        journal = input()
    print(writer.select(author, year=year, journal=journal))
    # writer.cursor.execute('update Articles set id = 1 where id = 0')  # для примера того, что SQL инъекция не работает
    return


main()
