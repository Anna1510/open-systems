# Лабораторная работа № 6

### Работа с SQL

## Описание работы

* Создать БД и все необходимые таблицы из скрипта
* Заполнить все таблицы данными из лабораторной работы № 5
* Выполнить запрос: вывести все статьи определённого автора
* *Опционально*: наложить доп. ограничения по году, журналу

## Особенности реализации

* Имеется иерархия классов, в ней **Record** - базовый класс для всех видов записей, **Article**, **Book**, **Booklet**, **PhdThesis**, **Conference** - его наследники, которые включают в себя специфические для каждого вида записи поля

```python
class Record:
    """
    Base class for all records
    """
    def write(self):
        return ''
```

```python
class Article(Record):
    """
    Class for article record
    """
    def write(self):
        """
        Forms an sql string for inserting an article record to a db
        :return: string
        """
        return f'INSERT INTO Articles (id, author, title, journal, address, year, volume, pages) VALUES ' \
               f'({self.id}, \'{self.author}\', \'{self.title}\', \'{self.journal}\', \'{self.address}\', {self.year}, ' \
               f'\'{self.volume}\', \'{self.pages}\')'

    def __init__(self, field_values, it):
        """
        Constructor for Article class
        :param field_values: dictionary where key - name of field, value - value of this field
        :param it: index of item (used for id)
        """
        self.id = it
        self.author = field_values['Author']
        self.title = field_values['Title']
        self.journal = field_values['Journal']
        self.address = field_values['Address']
        self.year = field_values['Year']
        while not self.year.isdigit():
            self.year = self.year[1:]
        self.volume = field_values['Volume']
        self.pages = field_values['Pages']
        return
```

```python
class Book(Record):
    def write(self):
        """
        Forms an sql string for inserting an article record to a db
        :return: string
        """
        return f'INSERT INTO Books (id, author, title, publisher, address, year, numpages) VALUES ' \
               f'({self.id}, \'{self.author}\', \'{self.title}\', \'{self.publisher}\', \'{self.address}\', {self.year},' \
               f' \'{self.numpages}\')'

    def __init__(self, field_values, it):
        """
        Constructor for Book class
        :param field_values: dictionary where key - name of field, value - value of this field
        :param it: index of
        """
        self.id = it
        self.author = field_values['Author']
        self.title = field_values['Title']
        self.publisher = field_values['Publisher']
        self.address = field_values['Address']
        self.year = field_values['Year']
        while not self.year.isdigit():
            self.year = self.year[1:]
        self.numpages = field_values['Numpages']
        return
```

```python
class Booklet(Record):
    def write(self):
        """
        Forms an sql string for inserting an article record to a db
        :return: string
        """
        return f'INSERT INTO Booklets (id, title) VALUES ({self.id}, \'{self.title}\')'

    def __init__(self, field_values, it):
        """
        Constructor for Booklet class
        :param field_values: dictionary where key - name of field, value - value of this field
        :param it: index of
        """
        self.id = it
        self.title = field_values['Title']
        pass
```

```python
class PhdThesis(Record):
    def write(self):
        """
        Forms an sql string for inserting an article record to a db
        :return: string
        """
        return f'INSERT INTO PhdTheses (id, author, title, school, address, year) VALUES ({self.id}, \'{self.author}\', ' \
               f'\'{self.title}\', \'{self.school}\', \'{self.address}\', {self.year})'

    def __init__(self, field_values, it):
        """
        Constructor for PhdThesis class
        :param field_values: dictionary where key - name of field, value - value of this field
        :param it: index of
        """
        self.id = it
        self.title = field_values['Title']
        self.author = field_values['Author']
        self.school = field_values['School']
        self.year = field_values['Year']
        while not self.year.isdigit():
            self.year = self.year[1:]
        self.address = field_values['Address']
        return
```

```python
class Conference(Record):
    def write(self):
        """
        Forms an sql string for inserting an article record to a db
        :return: string
        """
        return f'INSERT INTO Conferences (id, author, title, booktitle, address, year, pages) VALUES ' \
               f'({self.id}, \'{self.author}\', \'{self.title}\', \'{self.booktitle}\', \'{self.address}\', ' \
               f'{self.year}, \'{self.pages}\')'

    def __init__(self, field_values, it):
        """
        Constructor for Conference class
        :param field_values: dictionary where key - name of field, value - value of this field
        :param it: index of
        """
        self.id = it
        self.title = field_values['Title']
        self.author = field_values['Author']
        self.booktitle = field_values['Booktitle']
        self.year = field_values['Year']
        while not self.year.isdigit():
            self.year = self.year[1:]
        self.address = field_values['Address']
        self.pages = field_values['Pages']
        return
```