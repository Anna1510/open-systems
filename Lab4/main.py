import vk

session = vk.AuthSession(app_id='7351345', user_login='*******',
                         user_password='********', scope='wall, audio, friends')
api = vk.API(session, v='5.35')


def get_id1_user():
    """
    Gets the user info from Vk with id=1
    :return: list of str
    """
    return api.users.get(user_id=1)


def make_post(message):
    """
    Make a post on a wll
    :param message: message to be posted
    :return: nothing
    """
    api.wall.post(message=message)
    return


def get_friends():
    """
    Returns a list of friends ids
    :return: list of str
    """
    return api.friends.get()


if __name__ == '__main__':
    user = get_id1_user()
    friends = get_friends()
    print(user)
    print(friends)
    make_post(input("Введите сообщение для поста: "))
