# Лабораторная работа № 4

### Работа с API ВКонтаке

## Описание работы

* Необходимо воспользоваться готовой библиотекой [vk](https://pypi.org/project/vk/)

* Получить имя пользователя с id = 1

* Получить список друзей

* Сделать пост на стене

## Особенности реализации

* Можно ввести в консоль то сообщение, которое хочется опубликовать на стене

* Список друзей представляет собой список их id. Если необходимо получить информацию о них (имя и фамилия), то можно слегка модифицировать функцию

```python
def get_id1_user():
    """
    Gets the user info from Vk with id=1
    :return: list of str
    """
    return api.users.get(user_id=1)
```

В ней необходио передавать параметр id - id того пользователя, которого мы хотим получить. И вызывать метод API с данным параметром. Тогда функция будет выглядеть так:

```python
def get_id1_user(id):
    """
    Gets the user info from Vk with given id
    :return: list of str
    """
    return api.users.get(user_id=id)
```